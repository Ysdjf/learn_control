import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize

def yf(x):
    return x**2 + 2 * x + 1

print(optimize.fminbound(yf, -5, 5)) #-5与5之间最小值对应的x值
print(optimize.fmin(yf, -2)) #寻找最小值，第二个参数是初步猜测值
print(optimize.minimize_scalar(yf))
print(optimize.minimize(yf, -2)) #寻找最小值，一个或多个自变量，第二个参数是初步猜测值

x = np.arange(-5, 5, 0.5)
y = x**2 + 2 * x + 1
plt.plot(x, y)
plt.xlabel('x')
plt.ylabel('y')
plt.grid()
plt.show()