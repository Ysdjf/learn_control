import numpy as np
import matplotlib.pyplot as plt

x = np.arange(0, 2 * np.pi, 0.1)
y1 = np.sin(x)
y2 = np.cos(x)

plt.figure()
#plt.plot(x, y1, x, y2)
plt.plot(x, y1, 'g-', label = r'$\sin{x}$ line')
plt.plot(x, y2, 'b-.', label = r'$\cos{x}$ line')
plt.grid()
plt.xlabel(r'$x$')
plt.ylabel(r'$y$')
plt.title('figure1')
plt.legend()

plt.figure()
plt.subplot(2, 1, 1)
plt.plot(x, y1, 'g-')
plt.grid()
plt.xlabel(r'$x$')
plt.ylabel(r'$\sin{x}$')
plt.subplot(2, 1, 2)
plt.plot(x, y2, 'b')
plt.grid()
plt.xlabel(r'$x$')
plt.ylabel(r'$\cos{x}$')

plt.show()