#实时绘图，更为复杂的使用matplotlib.animation.FuncAnimation
import numpy as np
import matplotlib.pyplot as plt

T = 5
a = -1 / T
x0 = 1

Ts = 1
x = np.zeros(26)
t = np.zeros(26)
x[0] = x0
t[0] = 0

plt.axis([0, 25, 0, 1])
for i in range(25):
    x[i + 1] = x[i] * (a * Ts + 1)
    t[i + 1] = t[i] + Ts
    plt.scatter(t[i], x[i])
    plt.pause(1)

plt.plot(t, x)
plt.xlabel('t')
plt.ylabel('x')
plt.grid()
plt.show()